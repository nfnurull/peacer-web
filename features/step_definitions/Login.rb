Given("Open the website {string}") do |string|
  @browser.manage.window.maximize
  @browser.navigate.to string
  sleep(1)
end

Then("Admin input registered Email") do
  @browser.find_element(:xpath,'//*[@id="app"]/div/div/div[1]/div/div[2]/div[4]/div/div/form/div[1]/div[1]/input').send_keys("azizmustikaaji@gmail.com")
end

Then("Admin input registered Password") do
  @browser.find_element(:xpath,'//*[@id="password-field"]').send_keys("kalkulus")
end

Then("Admin Click on Sign In button") do
  @browser.find_element(:xpath,'//*[@id="app"]/div/div/div[1]/div/div[2]/div[4]/div/div/form/div[4]/button').click
  sleep(2)
end

Then("Admin directed to Dashboard") do
  sleep(2)
  username_text = @browser.find_element(:id,"user-name").text
  puts"Admin: #{username_text}"
  assert_equal("Aziz Mustika Aji",username_text)
  sleep(1)
  
  @browser.find_element(:xpath,'//*[@id="page-title"]').click
  dashboard = @browser.find_element(:xpath,'//*[@id="main-wrapper"]/div[2]/div/div/div[1]/div/ol/li/a')
  if dashboard.displayed?
    puts"Peacer Dashboard Displayed"
  else
    puts"Peacer Dashboard Doesn't Displayed"
  end
  sleep(2)
end

Then("Admin Logout") do
  @browser.find_element(:xpath,'//*[@id="2"]/i').click
  @browser.find_element(:xpath,'//*[@id="main-wrapper"]/header/nav/div[2]/ul[3]/li[2]/div/ul/li/a').click
end

Then("Admin input unregistered Email") do
  @browser.find_element(:xpath,'//*[@id="app"]/div/div/div[1]/div/div[2]/div[4]/div/div/form/div[1]/div[1]/input').send_keys("mustika@gmail.com")
end

Then("Admin Do not input Password") do
  @browser.find_element(:xpath,'//*[@id="password-field"]').click
end

Then("alert appears") do
  alert_text = @browser.find_element(:xpath,'//*[@id="app"]/div/div/div[1]/div/div[2]/div[4]/div/div/form/div[2]/div[2]').text
  puts"Alert: #{alert_text}"
  assert_equal("Password is required",alert_text)
end