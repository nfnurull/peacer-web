@PeacerLoginPage
Feature: Login Web Admin
  
  @PositiveLogin
  Scenario: Login Website Admin Positive
  Given Open the website "https://peacer-fe.netlify.com/"
  Then Admin input registered Email
  Then Admin input registered Password
  Then Admin Click on Sign In button
  Then Admin directed to Dashboard
  Then Admin Logout

  @NegativeLogin
  Scenario: Login Website Admin Negative
  Given Open the website "https://peacer-fe.netlify.com/"
  Then Admin input unregistered Email
  Then Admin Do not input Password
  Then Admin Click on Sign In button
  Then alert appears